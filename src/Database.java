import java.sql.Timestamp;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

public class Database {
    private HashMap<Integer, Record> recordMap = new HashMap<>();

    public void addRecord(Integer id, String name){
        try {
            recordMap.put(id, new Record(id, name));
            System.out.println("Record added.");
        }catch (NullPointerException npe){
            npe.printStackTrace();
        }
    }

    public Record getRecord(Integer id){
        return recordMap.get(id);
    }

    public void dataRefresh(){
        HashSet<Integer> toRemove = new HashSet<>();

        for (Map.Entry<Integer, Record> recordEntry :recordMap.entrySet()) {
            if(Duration.between(recordEntry.getValue().getAddTime(),
                    LocalDateTime.now()).compareTo(recordEntry.getValue().getDuration()) > 0) {
                toRemove.add(recordEntry.getKey());
            }
        }
        for (Integer id : toRemove) {
            recordMap.remove(id);
            System.out.println("Removed outdated record " + id);
        }
    }
}
