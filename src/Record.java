import java.sql.Timestamp;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class Record {
    private LocalTime addTime;
    private Duration duration = Duration.ofMillis(7000);
    private Integer caseID;
    private String caseName;


    public Record(Integer caseID, String caseName) {
        this.addTime = LocalTime.now();
        this.caseID = caseID;
        this.caseName = caseName;
    }

    public LocalTime getAddTime() {
        return addTime;
    }

    public Duration getDuration() {
        return duration;
    }

    public String getCaseName() {
        return caseName;
    }

    public Integer getCaseID() {
        return caseID;
    }

    @Override
    public String toString() {
        return "Record '" + caseName + "'\n ID: " + caseID + "\n created: " + addTime;
    }
}
