import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Database database = new Database();
        Scanner skan = new Scanner(System.in);
        String inputLine;

        printOptions();

        // add / search[ID] / refresh
        do {
            inputLine = skan.nextLine();
            if(inputLine.equals("quit")){
                break;
            }
            else if(inputLine.equals("refresh")){
                database.dataRefresh();
            }
            else{
                try {
                    String[] inputArguments = inputLine.split(" ");
                    if(inputArguments[0].equals("add")){
                        Integer caseID = Integer.parseInt(inputArguments[1]);
                        String name = inputArguments[2];
                        database.addRecord(caseID, name);
                    }
                    else if(inputArguments[0].equals("search")) {
                        Integer caseID = Integer.parseInt(inputArguments[1]);
                        if (database.getRecord(caseID) != null){
                            System.out.println(database.getRecord(caseID));
                        }
                        else {
                            System.out.println("Record ID: " + caseID + " does not figure in our database.");
                        }
                    }
                    else {
                        System.out.println("Wrong command. Please try again.");
                        System.out.println();
                        printOptions();
                    }
                }catch (ArrayIndexOutOfBoundsException aioobe){
                    System.out.println("Not enough information.");
                }catch (NumberFormatException nfe){
                    System.out.println("Case ID format could not be converted to a number. Please try again.");
                }
            }
            try {
                Runtime.getRuntime().exec("clear");
            } catch (IOException e) {
                e.printStackTrace();
            }

        }while (!inputLine.equals("quit"));

    }

    private static void printOptions() {
        System.out.println("Enter 'add' + recordID + recordName -> to add new record");
        System.out.println("Enter 'search' + recordID -> to search the database");
        System.out.println("Enter 'refresh' -> to delete expired database entries.");
        System.out.println("Type 'quit' to terminate the process.");
    }
}
